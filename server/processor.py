import os
import pickle
import shutil

import face_recognition
from flask import Flask, request, redirect, url_for, flash, jsonify
from gcm import GCM

from werkzeug.utils import secure_filename


UPLOAD_FOLDER = 'uploaded_files'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
GCM_API_KEY = "AIzaSyBHHdNOGCdlxv9k9IHl0jYDfc0IzQOgESo"

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

users = {
    "155135":	{"name": "Anubhab Goel", "token": "fQB7hT9C7J8:APA91bE2mNls4inA_y_MSXfTWhtaPT5HppDG89jP-4bjsQQ0ecETXhlhOZ80qM-gg_y6eoy-qXoHOWEMPExiG6PrNS8XRjBmtkhaST1TqG1vbOCmwih4AXp5w5csZ6oxQ4xAQgsShHY1"},
    "155136":	{"name": "Sanjay Jain", "token": "cecVUBh7Wi0:APA91bFS89tHdS9gFWHIUBqXHF9TCnNspCmm5sja5nOd1uOVBRRu09okWC0BOD0mHl3zgH6bIX3beafOyburaekXevDsWgEmme7NrnTOFav_0bcViRyZCVAIlSFyoVgyDP5h8cMFUwXL"},
    "155137":	{"name": "Nadeem Shaikh", "token": "8D6CA7C52913F370200F2BAF699ED76365E2312621915BC5D9B245F4B7607660"},
    "155138":	{"name": "Harshit Jain", "token": "cw88JjbmozI:APA91bEDwyTktTOrtkzaiS57MKgvRMX3MUVIFZSC8qprtLDy6YELnz8LxynnmX6B-z2LtptY5_2mmZlBiHnHp56jVPtYqtcv3STxFhk838Wf7mmO6N72W1OKCeT_BbLLoiX5CTigCkiF"},
    "156103":	{"name": "Mehul Agarwal", "token": "DDF48DB00B4C8AF57E03B26BC7F553E4F6783557EDCF6677AD74120405F86797"},
    "156104":	{"name": "Nikhil Jain", "token": "dqPCrVPX-1Y:APA91bHIVw27GaOOj3NArAI_3WYmmOAD0lWYiwcwoTENzV5Q44815Ux846aodBFcOrdRafDfiOnANbqesqB6X-bVflT6YyzZ1E-YGwPdiSN7gSbl1VUprOMt08YrNy4RJFgKkgc3Xb3V"},
    "159091":	{"name": "Priyesh Suryavanshi", "token": "e9HYKe3QYdk:APA91bGJzpa9LPrEeen2wOkwZtHquhsIqdD35K09e2lr1youJ6lMoJyvzy-vOqzzKJBfnhFZ5pnUtVB3X8NErxWW-wYHYrN1L2eYkUqcnPk23DYbMvbYL6V0bN9WRsRIHDacZ0CtxQg3"},
    "159354":	{"name": "Franklin", "token": "eap3d_EJ5vI:APA91bEMq84XimMw2tl_5RQ0-dcPbIHC0z8mZEtgg97I2gRdv4RySFbQB2S5JR-XKIvu8CXxeUxUWZ_65UyBW9z25jrzrwcsxpjxLAhcGSPNen0EwVrZnb0NvVUmkQ3gwOwjZpytLBeL"},
    "159451":	{"name": "Prashant Poojary", "token": "c-euQAsYRoM:APA91bFF8rhS7oy8Jy6P4OX3WiD18KToofmXPtZ0cxZv6Gcjd9vLbPulPhw25L92BuqPkKjpX210heKZL99IXr27YRcV1ve83TBM83Ji2930QmQcedoUIdTn-wjCkz_GVAtM3WLCencg"},
    "160220":	{"name": "Udit Kumar Bhola", "token": "cWxsqNzhM6U:APA91bEj-GfIagCVQ-rBxvdSF2xpGGTy0VIdgjTSlmhOHLbNa8eTUEoSTBEQCF_MPh_9c420O9wWpws8rnYOU-LhM2o3XMiwTLC3RWUbLWrs_bRq4o8TtgqtwZIsITgQwvqr_NpZcnm8"},
    "160850":	{"name": "Vikas Shinde", "token": "f4XNrbuV1uo:APA91bED2SUgY0peq1x_rCpCvt24vjSw6j--jhgzYhzfxZZBEB-HhsHY-GUHQ_9Tpmv8VMmXMbQMbInctCN_ZJ_QxQp-yJPD86Asn3yPLtjhTJHrPm4C0x1CqUY9VJWp3ha5IISu6B_a"},
    "161549":	{"name": "Sohel Patel", "token": "fhUmR8XbwXs:APA91bG91MKj1Rm32LJ1bh5z1GnGWNz2kb9wwT908IRuaLZdAuSppFPwLg53CUTN-U8t_FZn-CCvDGX3hQzJ8fQVqs8GpbYgD7ohO3l-YVZvkuFvQzwDFZf-weg9KxaOyTmNtFrCQnDM"},
    "161558":	{"name": "Ankit Bareja", "token": "1DA56AAAE7238AC78BF459E4DE4EDC131404A5223B1199F4972886DE4034DCBA"},
    "161569":	{"name": "Aizaz Rafiqi", "token": "14E3A068F16AE2AFB6CADD74998D2266FA1E12B790926C7913F97FF7CAD89693"},
    "163225":	{"name": "Gauraw Mishra", "token": "B3FB1E289E83ACEAC55DB57503DFA44D8F1794DF7B0D4FB3E2780B80014444BA"},
    "163671":	{"name": "Uday Kale", "token": "frxed2CvWiw:APA91bFre8jCXW0JzAjHXQ09P_xYcUv5ZLzg0KPWgLVbHCVTqbn0G8Souwlnwh9H0xIzXX6UIO_ncZrr-j7YyNr4A8UetxT0UxwIWwZRle49vfe895OH_3ZL9m1Rz1w4fNaLFPZdusZK"},
    "163959":	{"name": "Ankit Goyal", "token": "fopwcz7FNKU:APA91bEghKJGpxzmBnO4Or2sF2ovsG8Jt3GEo9DX3Wsu4lqbarCNxtT5RAIRDLHaUmFiQy_6fKt6HRsEf1cohOSHeoaBb861Qk8t45Q0NsCW0hJ6TPsvzTbP9uLkg4-jUyJqj3OTWF6B"},
    "164261":	{"name": "Rahul Kotian", "token": "cru5MY4IYW0:APA91bGcsTBi8ZNaleixNGlZXL3XTzgeqWot8yWmemkIOxj0WZdO_6UXhJOfzsNfFRoJxsAu-VF5d7oOdFN2ut2UTYYrH0MkYGn7z62nJ9usREjhe-ntxzRROgmZyfcDvTet-8eWdLt2"},
    "164395":	{"name": "Vinisha Katariya", "token": "0654BD408FBB1AEB285640EB6D09016A710D0D8DFB450E6497B4C2F03E18CAC1"},
    "166630":	{"name": "Nikunj Sharma", "token": "chUBYVQzo6w:APA91bHP0FBhkiONmVDnuOZS1ZwwC_uIB7eg6zKGc_0bSBfK1rlOqjqUApTYvLGAuNqg_MHaFzhXrLjloJBFuc0bGwTswt8aoE9Itatct4aGP7mvntRqW-WmMSZa-q3xL9WelvHaxGD2"},
    "166850":	{"name": "Fatema Githam", "token": "eYv90cExz94:APA91bGzogLzqyzAobdW5wQaMsYELc_Exn639qq45DolJZXh2FMtdhzFCgRQ23Ub-yxlWjlHAgqu8HIQSSOVqMWtseznPHqrmDPj8Jb7RH4RsZMHji4XSPSYm_Ye6KFdcPg62iROEu7u"},
    "166851":	{"name": "Sachin Talekar", "token": "fIRmhdpKt4Q:APA91bG3s-8bqUg4o9FwfkOZuSKqVcU1lnm0jQcZqrA-FHVAEnDW5DtvP60m4YYCTf7Sl3jFtH6TFXc93WpEszKBh39SM2xnLlf8AYqlzq6vgh3MFt01f9VEPtatfN078zzLbOPSMeLy"}
}


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def predict(X_img_path, knn_clf=None, model_path=None, distance_threshold=0.5):
    if not os.path.isfile(X_img_path) or os.path.splitext(X_img_path)[1][1:] not in ALLOWED_EXTENSIONS:
        raise Exception("Invalid image path: {}".format(X_img_path))

    if knn_clf is None and model_path is None:
        raise Exception("Must supply knn classifier either thourgh knn_clf or model_path")

    # Load a trained KNN model (if one was passed in)
    if knn_clf is None:
        with open(model_path, 'rb') as f:
            knn_clf = pickle.load(f)

    # Load image file and find face locations
    X_img = face_recognition.load_image_file(X_img_path)
    X_face_locations = face_recognition.face_locations(X_img)

    # If no faces are found in the image, return an empty result.
    if len(X_face_locations) == 0:
        return []

    # Find encodings for faces in the test iamge
    faces_encodings = face_recognition.face_encodings(X_img, known_face_locations=X_face_locations)

    # Use the KNN model to find the best matches for the test face
    closest_distances = knn_clf.kneighbors(faces_encodings, n_neighbors=1)
    are_matches = [closest_distances[0][i][0] <= distance_threshold for i in range(len(X_face_locations))]

    # Predict classes and remove classifications that aren't within the threshold
    return [(pred, loc) if rec else ("unknown", loc) for pred, loc, rec in
            zip(knn_clf.predict(faces_encodings), X_face_locations, are_matches)]


# if __name__ == "__main__":
def find_user():
    ids = []
    for image_file in os.listdir(UPLOAD_FOLDER):
        full_file_path = os.path.join(UPLOAD_FOLDER, image_file)

        print("Looking for faces in {}".format(image_file))

        predictions = predict(full_file_path, model_path="trained_knn_model.clf")

        print(predictions)

        for name, (top, right, bottom, left) in predictions:
            print("- Found {} ".format(name, left, top))
            ids.append(name)

    return ids


def send_notification(ids):
    gcm = GCM(GCM_API_KEY)
    for id in ids:
        user = users.get(id)
        # Downstream message using JSON request
        if id in users:
            reg_ids = [user.get('token')]
            data = {"message": "Hi " + user.get('name') + " Your attendance is marked! (TESTING)"}
            response = gcm.json_request(registration_ids=reg_ids, data=data)
            print(response)


@app.route('/')
def hello_world():
    return 'Hey there, Good to see you!'


@app.route('/v1/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return jsonify({'message': 'File format is not supported'}), 400
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return jsonify({'message': 'no file found.'}), 404
        if file and allowed_file(file.filename):
            if os.path.isdir(UPLOAD_FOLDER):
                shutil.rmtree(UPLOAD_FOLDER)
            os.mkdir(UPLOAD_FOLDER)
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            ids = find_user()
            send_notification(ids)
            return jsonify({"message": 'successfully uploaded file: ' + filename, "names": ids}), 200
            # return redirect(url_for('uploaded_file',filename=filename))
    return '''
        <!doctype html>
        <title>Upload new File</title>
        <h1>Upload new File</h1>
        <form method=post enctype=multipart/form-data>
          <p><input type=file name=file>
             <input type=submit value=Upload>
        </form>
        '''


if __name__ == "__main__":
    app.secret_key = 'adnxiengk335kdnfd'
    app.config['SESSION_TYPE'] = 'filesystem'
    app.debug = True
    app.run(host='0.0.0.0', port=5000, debug=True)

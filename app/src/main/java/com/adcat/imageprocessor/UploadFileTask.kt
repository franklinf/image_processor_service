package com.adcat.imageprocessor

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.util.Base64
import android.util.Log
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URI
import java.net.URL
import java.io.*




/**
 * Created by franklinf on 12-10-2017.
 */
class UploadFileTask(private var filePathList: String, private var callBack: (response: JSONObject) -> Unit?) : AsyncTask<Void, Void, JSONObject>() {

    override fun doInBackground(vararg params: Void?): JSONObject {
        val resultObject = JSONObject()
        try {
            val lineEnd = "\r\n"
            val twoHyphens = "--"
            val boundary = "*****"
            var bytesRead: Int
            var bytesAvailable: Int
            var bufferSize: Int
            val buffer: ByteArray
            val maxBufferSize = 1024 * 1024
            //todo change URL as per client ( MOST IMPORTANT )
            val url = URL("http://13.127.142.215:5000/v1/upload")
            val connection = url.openConnection() as HttpURLConnection

            // Allow Inputs &amp; Outputs.
            connection.doInput = true
            connection.doOutput = true
            connection.useCaches = false

            // Set HTTP method to POST.
            connection.requestMethod = "POST"
            connection.setRequestProperty("Connection", "Keep-Alive")
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=$boundary")
            val fileInputStream: FileInputStream
            val outputStream: DataOutputStream
            outputStream = DataOutputStream(connection.outputStream)
            outputStream.writeBytes(twoHyphens + boundary + lineEnd)

            outputStream.writeBytes("Content-Disposition: form-data; name=\"reference\"$lineEnd")
            outputStream.writeBytes(lineEnd)
            outputStream.writeBytes("my_refrence_text")
            outputStream.writeBytes(lineEnd)
            outputStream.writeBytes(twoHyphens + boundary + lineEnd)

            outputStream.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\"" + filePathList.substring(filePathList.lastIndexOf("/")+1) + "\"" + lineEnd)
            outputStream.writeBytes(lineEnd)

            fileInputStream = FileInputStream(filePathList)
            bytesAvailable = fileInputStream.available()
            bufferSize = Math.min(bytesAvailable, maxBufferSize)
            buffer = ByteArray(bufferSize)

            // Read file
            bytesRead = fileInputStream.read(buffer, 0, bufferSize)

            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bufferSize)
                bytesAvailable = fileInputStream.available()
                bufferSize = Math.min(bytesAvailable, maxBufferSize)
                bytesRead = fileInputStream.read(buffer, 0, bufferSize)
            }
            outputStream.writeBytes(lineEnd)
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd)

            // Responses from the server (code and message)
            val serverResponseCode = connection.responseCode
            fileInputStream.close()
            outputStream.flush()
            outputStream.close()

            var br: BufferedReader

            br = if (connection.responseCode in 200..299) {
                BufferedReader(InputStreamReader(connection.inputStream))
            } else {
                BufferedReader(InputStreamReader(connection.errorStream))
            }

            var response = String()
            var line: String = br.readLine()
            while (line != null) {
                response += line
            }
            return JSONObject(response)
//            if (serverResponseCode == 200) {
//                resultObject.put("message", "Successfully uploaded file")
//            } else {
//                resultObject.put("message", "Sorry some problem occurred, could not upload file")
//            }

        } catch (e: Exception) {
            e.printStackTrace()
            resultObject.put("message", "Sorry some problem occurred, could not upload file")
        }
        return resultObject
    }

    override fun onPostExecute(result: JSONObject) {
        super.onPostExecute(result)
        callBack(result)
    }

}
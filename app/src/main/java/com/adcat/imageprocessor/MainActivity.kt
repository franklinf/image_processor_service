package com.adcat.imageprocessor

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {

    private val REQUIRED_STORAGE_PERMISSIONS = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
    private val PERMISSION_ALL_STORAGE_REQUEST_CODE = 100
    val CAPTURE_IMAGE = 1001
    lateinit var mCurrentPhotoPath: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        takePicture.setOnClickListener { takePictureWithCamera() }
        uploadPicture.setOnClickListener { uploadToServer() }
    }

    private fun takePictureWithCamera() {
        if (isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE) && isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            takePhoto()
        } else {
            ActivityCompat.requestPermissions(this, REQUIRED_STORAGE_PERMISSIONS, PERMISSION_ALL_STORAGE_REQUEST_CODE)
        }
    }

    private fun uploadToServer() {
        UploadFileTask(mCurrentPhotoPath, {
            showSnackBar(it.optString("message"))
        }).execute()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_ALL_STORAGE_REQUEST_CODE -> {
                if (isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE) && isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    takePhoto()
                } else {
                    showSnackBar("Media storage permission required")
                }
            }
        }
    }

    /**
     * Method to check app has permission to receive sms.
     *
     * @return boolean isPermissionGranted
     */
    fun isPermissionGranted(permission: String): Boolean {
        val permissionCheck = ContextCompat.checkSelfPermission(this, permission)
        return permissionCheck == PackageManager.PERMISSION_GRANTED
    }

    private fun showSnackBar(text: String) {
        val mySnackbar = Snackbar.make(parentLayout, text, Snackbar.LENGTH_INDEFINITE)
        mySnackbar.show()
    }

    private fun takePhoto(){
        // bug in some devices.
        // https://stackoverflow.com/questions/5059731/get-image-uri-in-onactivityresult-after-taking-photo
        // startActivityForResult(Intent(MediaStore.ACTION_IMAGE_CAPTURE), CAPTURE_IMAGE);

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            val photoFile = createImageFile();
            // Continue only if the File was successfully created
            // /storage/emulated/0/Android/data/com.hdfclife.geofencing.uat/files/Pictures/JPEG_20171219_174148_1676759716.jpg
            if (photoFile != null) {
                val photoURI = FileProvider.getUriForFile(this, "${BuildConfig.APPLICATION_ID}.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAPTURE_IMAGE);
            }
        }
    }


    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.absolutePath
        return image
    }


    // Add the file to galary
    private fun galleryAddPic() {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(mCurrentPhotoPath)
        val contentUri = Uri.fromFile(f)
        mediaScanIntent.data = contentUri
        this.sendBroadcast(mediaScanIntent)
    }

    /**
     * The image size to too big, could not upload to server,
     * Compressing it a bit.
     */
    fun compressCapturedImage(filePath: String){
        // BitmapFactory options to downsize the image
        val o =  BitmapFactory.Options()
        o.inJustDecodeBounds = true;
        o.inSampleSize = 6;
        // factor of downsizing the image
        val file = File(filePath)
        var inputStream = FileInputStream(file)
        //Bitmap selectedBitmap = null;
        BitmapFactory.decodeStream(inputStream, null, o);
        inputStream.close();

        // The new size we want to scale to
        val REQUIRED_SIZE=75;

        // Find the correct scale value. It should be the power of 2.
        var scale = 1;
        while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                o.outHeight / scale / 2 >= REQUIRED_SIZE) {
            scale *= 2;
        }

        val o2 =  BitmapFactory.Options();
        o2.inSampleSize = scale;
        inputStream =  FileInputStream(filePath)

        val selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2)
        inputStream.close()
        /*val matrix = Matrix()
        matrix.postRotate(90.0f)
        val rotatedBitmap = Bitmap.createBitmap(selectedBitmap, 0, 0, selectedBitmap.getWidth(), selectedBitmap.getHeight(), matrix, true)*/

        val rotatedBitmap = getRotatedImage(filePath, selectedBitmap)

        // here i override the original image file
        file.createNewFile()
        val outputStream = FileOutputStream(file)
        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream)
        outputStream.close()
    }


    private fun getRotatedImage(photoPath: String, bitmap: Bitmap) : Bitmap {
        val ei =  ExifInterface(photoPath)
        val  orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)

        return when(orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> {
                rotateImage(bitmap, 90F)
            }
            ExifInterface.ORIENTATION_ROTATE_180 -> {
                rotateImage(bitmap, 180F)
            }
            ExifInterface.ORIENTATION_ROTATE_270 -> {
                rotateImage(bitmap, 270F)
            }
            else -> {
                bitmap
            }
        }
    }

    private fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
    }

    private fun shouldCompressTheImage(path: String): Boolean {
        val file = File(path)
        return  file.length() > 2048
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode === Activity.RESULT_OK) {
            when(requestCode){
                CAPTURE_IMAGE -> {
                    /*val photo = data?.extras?.get("data") as Bitmap
                    if(null != photo) {
                        val bytes = ByteArrayOutputStream()
                        photo.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                        val path = Images.Media.insertImage(contentResolver, photo, "ExpenseManagementImage", "This is temp image, used for storing the image just before pushing it to server")
                        var absolutePath  = MConnectPathUtils.getPath(Uri.parse(path))
                        fileSelectionActivityInteractionListener.onImageCapturedFromCamera(absolutePath!!)
                    } else {

                    }*/
                    // we have received success image, now lets send update the ui
                    compressCapturedImage(mCurrentPhotoPath)
                    galleryAddPic()
                }
                else -> {}
            }

        }
    }
}

# README #

### This project contains 
	* An Android appliation
	* Server code

### What is this repository for? ###
	* This repo is a demonstrates how face recognization can be worked with MConnenct app for ADCAT employees. 

### How do I get set up? ###

	* For android, Open the project in android studio and select app as module and run the project. 
	* For Server, Open terminal and navicate to server folder. 
		* Install the following Python3 dependencies. 
			* Flask (Flask, request, redirect, url_for, flash, jsonify)
			* Pickle
			* Numpy
			* face_recognition
			* GCM
			* Opencv
	* We are not using any db now, currently all the empId, name, GCM tokens are put in a Dictionary. 
	* This project runs on python3 or above, since the model requires it. 
	* The face_recognition modle file is placed under server/trained_knn_model.clf file. 
	* After installing all the dependncies run sudo python3 processor.py to start the server. 
	* Then open http://localhost:5000/v1/upload and upload an image to check whether it detects the user or not.
	* Alternativly open the android app and capture an image and try uploading 
	* Happy Coding :)

### Contribution guidelines ###

	* Fork it, add your changes, Raise a PR. 

### Who do I talk to? ###

	* root :)